BIN := _bin/libhandymacros.so
SRC := $(wildcard src/*.c)
OBJ := $(SRC:src/%.c=_obj/%.o)
DEPFILES := $(OBJ:_obj/%.o=_obj/deps/%.dep)
TESTSRC := $(wildcard test/*.c)
TESTEXE := $(TESTSRC:test/%.c=_test/c%) _test/c++test_c99 _test/c++test_c11
HEADERS := $(wildcard src/*.h)

C_CXX_FLAGS = -O2 -Wpedantic -Wall -Wextra -Wcast-qual -Wno-missing-field-initializers
CFLAGS = -std=c99 $(C_CXX_FLAGS)
CXXFLAGS = -std=c++11 $(C_CXX_FLAGS) -x c++
DEPFLAGS = -MT $@ -MMD -MP -MF _obj/deps/$*.dep

prefix := /usr/local
includedir := $(prefix)/include
libdir := $(prefix)/lib


.PHONY: all test run-test clean install uninstall

all: $(BIN)

test: $(BIN) $(TESTEXE)

clean:
	rm -rf _bin _obj _test

$(BIN): $(OBJ) | _bin
	$(LINK.o) -o $@ $(OBJ) -shared
	chmod -x $@

_bin _obj _obj/deps _test:
	mkdir -p $@

_obj/%.o: src/%.c | _obj _obj/deps
	$(COMPILE.c) -o $@ $(DEPFLAGS) -fPIC $<

_test/ctest_c23: CFLAGS += -Wno-override-init -std=c2x
_test/ctest_c11: CFLAGS += -std=c11
_test/ctest_%: test/test_%.c test/test.h $(BIN) | _test
	$(LINK.c) -o $@ -iquote src $< -L_bin -lhandymacros
_test/c++test_c99: CXXFLAGS += -std=c++20
_test/c++test_%: test/test_%.c test/test.h $(BIN) | _test
	$(LINK.cc) -o $@ -iquote src $< -L_bin -lhandymacros

$(DEPFILES):

include $(DEPFILES)

.ONESHELL:

run-test: $(if $(test),_test/$(test),$(TESTEXE))
	@
	if [ "$(test)" ]; then
		LD_LIBRARY_PATH=_bin _test/$(test)
	else
		for test in $(TESTEXE); do
			LD_LIBRARY_PATH=_bin ./"$$test"
		done
	fi

install: $(BIN) $(HEADERS)
	mkdir -p $(DESTDIR)$(includedir) $(DESTDIR)$(libdir)
	cp -f $(HEADERS) $(DESTDIR)$(includedir)
	cp -f $(BIN) $(DESTDIR)$(libdir)

uninstall:
	$(RM) $(HEADERS:src/%.h=$(DESTDIR)$(includedir)/%.h)
	$(RM) $(BIN:_bin/%=$(DESTDIR)$(libdir)/%)
