#include "handymacros.h"


FUNC extern inline void _hm_realloc(void * * ptr_addr, size_t size);


FUNC extern inline void swap_blocks(void * val1, void * val2, size_t size);


FUNC extern inline long index_of_as_ptr(void const * value, void const * array, size_t value_size, size_t nb_elems);

FUNC extern inline long index_in_str_array(char const * str, char const * const array[], size_t nb_elems);


FUNC bool has_prefix(char const * str, char const * prefix)
{
   while (*str != '\0' && *prefix != '\0' && *str == *prefix) {
      ++str;
      ++prefix;
   }
   return *prefix == '\0';
}

FUNC extern inline bool has_suffix(char const * str, char const * suffix);

FUNC extern inline char * hm_strdup(char const * str);


DefComparator(int, i)
DefComparator(long, l)
DefComparator(float, ff)
DefComparator(double, f)
DefComparator(long double, fl)
DefComparator(size_t, z)

DefMinMax(int, i)
DefMinMax(long, l)
DefMinMax(size_t, z)


/*--- Containers ---*/

FUNC void * _array_of_list(void const * list, size_t item_size, size_t offset)
{
   size_t len = list_len(list);
   NewPtrDim(byte, array, [len], [item_size]);
   Cast(ListConst(byte), list_alias, list);
   for (size_t value_num = 0; value_num < len; ++value_num) {
      memcpy(array[value_num], (byte const *)list_alias + offset, item_size);
      list_alias = list_alias->next;
   }
   return array;
}

FUNC extern inline void _slice_realloc(Slice(void) * slice_addr, size_t nb_elems, size_t item_size);
FUNC extern inline void _slice_assign(Slice(void) * slice_addr, SliceConst(void) * other_slice, size_t item_size);

FUNC extern inline void _slice_memset(Slice(void) * slice, int val, size_t item_size);

FUNC extern inline void _slice_qsort(Slice(void) * slice, size_t item_size, int (* comparator)(void const *, void const *));
FUNC extern inline void * _slice_bsearch(void const * value, SliceConst(void) * slice, size_t item_size,
      int (* comparator)(void const *, void const *));


FUNC void _dynarr_grow(DynArr(void) * dynarr_addr, size_t item_size)
{
   if (dynarr_addr->len > dynarr_addr->capac) {
      dynarr_addr->capac = zmax(dynarr_addr->len, (dynarr_addr->capac + 1) * 3 / 2);
      dynarr_addr->data = realloc(dynarr_addr->data, sizeof(byte[dynarr_addr->capac][item_size]));
   }
}

FUNC extern inline void _dynarr_shrink(DynArr(void) * dynarr_addr, size_t item_size);

FUNC extern inline void _dynarr_push(DynArr(void) * dynarr_addr, void const * values, size_t nb_vals, size_t item_size);

FUNC void _dynarr_pop(DynArr(void) * dynarr_addr, size_t item_size)
{
   --dynarr_addr->len;
   if (dynarr_addr->len < dynarr_addr->capac / 2) {
      dynarr_addr->capac = dynarr_addr->capac * 2 / 3;
      dynarr_addr->data = realloc(dynarr_addr->data, sizeof(byte[dynarr_addr->capac][item_size]));
   }
}


FUNC void _deque_realloc(Deque(void) * deque_addr, size_t old_neg_capac, size_t old_pos_capac,
      size_t new_neg_capac, size_t new_pos_capac, size_t item_size)
{
   void * buf = malloc(sizeof(byte[new_neg_capac + new_pos_capac][item_size]));
   memcpy((byte *)buf + new_neg_capac * item_size, (byte *)deque_addr->buf + old_neg_capac * item_size,
      zmin(new_pos_capac, old_pos_capac) * item_size);
   free(deque_addr->buf);
   deque_addr->capac = new_neg_capac + new_pos_capac;
   deque_addr->data = (byte *)buf + new_neg_capac * item_size;
   deque_addr->buf = buf;
}

FUNC void _deque_grow_after(Deque(void) * deque_addr, size_t item_size)
{
   size_t neg_capac = ((byte *)deque_addr->data - (byte *)deque_addr->buf) / item_size,
      pos_capac = deque_addr->capac - neg_capac;
   if (deque_addr->len > pos_capac) {
      size_t new_pos_capac = zmax(deque_addr->len, (deque_addr->capac + 1) * 3 / 2 - neg_capac);
      _deque_realloc(deque_addr, neg_capac, pos_capac, neg_capac, new_pos_capac, item_size);
   }
}
FUNC void _deque_grow_before(Deque(void) * deque_addr, size_t item_size)
{
   if ((byte *)deque_addr->data == (byte *)deque_addr->buf) {
      size_t neg_capac = ((byte *)deque_addr->data - (byte *)deque_addr->buf) / item_size,
         pos_capac = deque_addr->capac - neg_capac,
         new_neg_capac = (deque_addr->capac + 1) * 3 / 2 - pos_capac;
      _deque_realloc(deque_addr, neg_capac, pos_capac, new_neg_capac, pos_capac, item_size);
   }
}

FUNC void _deque_shrink(Deque(void) * deque_addr, size_t item_size)
{
   deque_addr->capac = deque_addr->len;
   deque_addr->data = memdup(deque_addr->data, sizeof(byte[deque_addr->capac][item_size]));
   free(deque_addr->buf);
   deque_addr->buf = deque_addr->data;
}

FUNC extern inline void _deque_push(Deque(void) * deque_addr, void const * values, size_t nb_vals, size_t item_size);

FUNC void _deque_pop(Deque(void) * deque_addr, size_t item_size)
{
   --deque_addr->len;
   size_t neg_capac = ((byte *)deque_addr->data - (byte *)deque_addr->buf) / item_size,
      pos_capac = deque_addr->capac - neg_capac;
   if (deque_addr->len * 2 < pos_capac) {
      size_t new_neg_capac = zmin(neg_capac, deque_addr->len / 2), new_pos_capac = pos_capac * 2 / 3;
      _deque_realloc(deque_addr, neg_capac, pos_capac, new_neg_capac, new_pos_capac, item_size);
   }
}

FUNC void _deque_shift(Deque(void) * deque_addr, size_t item_size)
{
   --deque_addr->len;
   size_t neg_capac = ((byte *)deque_addr->data - (byte *)deque_addr->buf) / item_size,
      pos_capac = deque_addr->capac - neg_capac;
   if (deque_addr->len < neg_capac + 1) {
      size_t new_neg_capac = neg_capac / 3, new_pos_capac = zmin(pos_capac, deque_addr->len * 3 / 2);
      _deque_realloc(deque_addr, neg_capac, pos_capac, new_neg_capac, new_pos_capac, item_size);
   }
   deque_addr->data = (byte *)deque_addr->data + item_size;
}

FUNC extern inline void _deque_unshift(Deque(void) * deque_addr, void const * value, size_t item_size);


FUNC void * _list_of_array(void const * array, size_t nb_elems, size_t item_size, size_t offset, size_t full_size)
{
   List(byte) list, * cur = &list;
   for (size_t value_num = 0; value_num < nb_elems; ++value_num) {
      *cur = (List(byte))malloc(full_size);
      memcpy((byte *)*cur + offset, (byte const *)array + value_num * item_size, item_size);
      cur = &(*cur)->next;
   }
   *cur = nullptr;
   return list;
}

FUNC extern inline void * _list_of_slice(SliceConst(void) slice, size_t item_size, size_t offset, size_t full_size);

FUNC extern inline void * * _list_cons(void const * item, size_t item_size, void * list_addr, size_t offset, size_t full_size);

FUNC extern inline void * * _list_decons(void * list_addr);

FUNC void * * _list_cat(void * list1_addr, void * list2)
{
   Cast(List(byte) *, list1, list1_addr);
   while (*list1 != nullptr) {
      list1 = &(*list1)->next;
   }
   *list1 = (List(byte))list2;
   return (void * *)list1_addr;
}

FUNC void list_free(void * list)
{
   Cast(List(byte), list_alias, list);
   while (list_alias != nullptr) {
      List(byte) next = list_alias->next;
      free(list_alias);
      list_alias = next;
   }
}

FUNC size_t list_len(void const * list)
{
   size_t len = 0;
   for (Cast(ListConst(byte), list_alias, list); list_alias != nullptr; list_alias = list_alias->next) {
      ++len;
   }
   return len;
}

FUNC size_t list_len_bounded(void const * list, size_t limit)
{
   Cast(ListConst(byte), list_alias, list);
   size_t len = 0;
   while (list_alias != nullptr && len < limit) {
      ++len;
      list_alias = list_alias->next;
   }
   return len;
}


FUNC void * _dict_get(void * data, size_t nb_elems, char const * key, size_t item_size, size_t offset)
{
   byte * elem = data;
   for (size_t pos = 0; pos < nb_elems; ++pos) {
      if (strcmp(key, *(char const * *)elem) == 0) {
         return elem + offset;
      }
      elem += item_size;
   }
   return nullptr;
}

FUNC void _dict_set(DynArr(void) * dict, char const * key, void * value, size_t item_size, size_t offset, size_t value_size)
{
   byte * elem = _dict_get(dict->data, dict->len, key, item_size, offset);
   if (elem != nullptr) {
      memcpy(elem + offset, value, value_size);
   } else {
      byte item[item_size];
      memcpy(item, &key, sizeof(key));
      memcpy(item + offset, value, value_size);
      _dynarr_push(dict, item, 1, item_size);
   }
}
