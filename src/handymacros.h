#ifndef HANDYMACROS_H
#define HANDYMACROS_H


#include <stddef.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdalign.h>
#include <limits.h>
#include <string.h>
#include <assert.h>


#ifndef __cplusplus
   #define ARRAY(type, ...) (type[])__VA_ARGS__
   #define LVAL_ARRAY(type, ...) (type[])__VA_ARGS__
   #define COMP_LIT(type, ...) (type)__VA_ARGS__
   #define FUNC
   #define sizeof_dims(type, dims) sizeof(type dims)
   #if defined __STDC_VERSION__ && __STDC_VERSION__ >= 202311L
      #define opt_cast(expr) (typeof(expr))
   #else
      #define opt_cast(expr)
      #define nullptr ((void *)NULL)
   #endif
   #if (!defined __STDC_VERSION__) || __STDC_VERSION__ < 201710L
      #undef true
      #undef false
      #define true ((bool)1)
      #define false ((bool)0)
   #endif

#else
   #include <vector>

   #define ARRAY(type, ...) std::initializer_list<type>(__VA_ARGS__).begin()
   #define LVAL_ARRAY(type, ...) std::vector<type>(__VA_ARGS__).data()
   #define COMP_LIT(type, ...) __VA_ARGS__
   #define FUNC extern "C"
   struct SizeofDims {
      size_t size;
      SizeofDims constexpr operator[](size_t size) {
         return SizeofDims{ this->size * size };
      }
   };
   #define sizeof_dims(type, dims) ((SizeofDims{ sizeof(type) } dims).size)
   #define typeof(expr) std::decay<decltype(expr)>::type
   #define opt_cast(expr) (std::decay<decltype(expr)>::type)
#endif


#define block(...) do { __VA_ARGS__ } while (false)
#define hm_end }

#define just_va_args(...) __VA_ARGS__
#define va_args(args) just_va_args args

#define head_arg(arg, ...) arg
#define tail_args(arg, ...) __VA_ARGS__


typedef struct { char byte; } byte;
typedef long double long_double;
#if 0
   typedef struct {} nothing_t;
#else
   typedef byte nothing_t;
#endif
#if !defined __STRICT_ANSI__ || (defined __STDC_VERSION__ && __STDC_VERSION__ >= 202311L) || defined __cplusplus
   #define HM_NOTHING_INIT {}
#else
   #define HM_NOTHING_INIT { 0 }
#endif

#define array_len(...) (sizeof(__VA_ARGS__) / sizeof(*(__VA_ARGS__)))
#define md_array_len(array, ref) (sizeof(array) / sizeof(ref(array)))
#ifndef __cplusplus
   #define lit_array_len(type, ...) (sizeof((type[])__VA_ARGS__) / sizeof(type))
#else
   #define lit_array_len(type, ...) std::initializer_list<type>(__VA_ARGS__).size()
#endif

#define lit_strlen(str) (sizeof(str) / sizeof(*(str)) - 1)

#define offsetof_inobj(obj, field) ((byte *)&(obj).field - (byte *)&(obj))
#define obj_of_field(addr, type, field) ((type *)((byte *)(addr) - offsetof(type, field)))

#define Cast(type, var_name, buffer) \
   type var_name = (type)(buffer)
#define PtrDim(type, var_name, sizes, buffer) \
   type (* var_name)sizes = (type (* )sizes)(buffer)
#define PtrDim2(type, var_name, size_comment, sizes, buffer) \
   type (* var_name)sizes = (type (* )sizes)(buffer)

#define ToStruct(buffer, var_name, ...) \
   struct StructFor_ ## var_name __VA_ARGS__ var_name = *(struct StructFor_ ## var_name *)(buffer)
#define Destruct(type, obj, decls, ...) \
   block({ type const * _obj = &(obj); va_args(decls) __VA_ARGS__ })
#define take(field_name) \
   typeof(_obj->field_name) field_name = _obj->field_name;
#define takeT(type, field_name) \
   type field_name = _obj->field_name;

#define Struct(StructName, ...) \
   typedef struct StructName StructName; \
   struct StructName __VA_ARGS__;

#define call(obj, ...) obj.func(obj.data, ## __VA_ARGS__)

#define ArgsFor(name) ArgsFor_ ## name
#define RetTypeOf(name) RetTypeOf_ ## name
#define DeclArgs(ret_type, name, args) \
   Struct(ArgsFor(name), { nothing_t _nothing; va_args(args); }) \
   typedef ret_type RetTypeOf(name);
#define DeclFunc(ret_type, name, args) \
   DeclArgs(ret_type, name, args) \
   FUNC RetTypeOf(name) _ ## name(ArgsFor(name) arg);
#define DeclFuncPtr(name) \
   RetTypeOf(name) (*_ ## name)(ArgsFor(name) arg);
#define DefFunc(name) \
   FUNC RetTypeOf(name) _ ## name(ArgsFor(name) arg)
#define DefFunc2(name, name2) \
   FUNC RetTypeOf(name2) _ ## name(ArgsFor(name2) arg)
#define Function(ret_type, name, args) \
   DeclArgs(ret_type, name, args) \
   FUNC RetTypeOf(name) _ ## name(ArgsFor(name) arg)
#define call_func(name, def_vals, user_vals) \
   _ ## name((ArgsFor(name)){ HM_NOTHING_INIT, va_args(def_vals), ._nothing = HM_NOTHING_INIT, va_args(user_vals) })
#define func_addr(name) &_ ## name
#define call_meth(obj, name, args) (obj)->methods->name(obj, va_args(args))
#define call_meth_noarg(obj, name) (obj)->methods->name(obj)


#define cnew(type, ...) (type *)malloc(sizeof(type __VA_ARGS__))
#define alloc(...) \
   (typeof(__VA_ARGS__) *)memcpy(cnew(typeof(__VA_ARGS__)), ARRAY(typeof(__VA_ARGS__), { __VA_ARGS__ }), sizeof(__VA_ARGS__))
#define alloc_array(...) \
   (typeof(*(__VA_ARGS__)) *)memcpy(cnew(typeof(__VA_ARGS__)), __VA_ARGS__, sizeof(__VA_ARGS__))
#define allocT(type, ...) \
   (type *)memcpy(cnew(type), ARRAY(type, { __VA_ARGS__ }), sizeof(type))
#define alloc_arrayT(type, ...) \
   (type *)memcpy(malloc(sizeof((type[])__VA_ARGS__)), ARRAY(type, __VA_ARGS__), sizeof((type[])__VA_ARGS__))

FUNC inline void _hm_realloc(void * * ptr_addr, size_t size)
{ *ptr_addr = realloc(*ptr_addr, size); }
#define hm_realloc(var_expr, ...) \
   _hm_realloc((void * *)(var_expr), sizeof_dims(byte, __VA_ARGS__[sizeof(**(var_expr))]))

#define hm_memset(array, value, size) \
   memset(array, value, size * sizeof(*(array)))
#define hm_memcpy(dest, src, nb_elems) \
   (opt_cast(&*(dest)) memcpy(dest, src, (nb_elems) * sizeof(*dest)))
#define hm_memmove(dest, src, nb_elems) \
   (opt_cast(&*(dest)) memmove(dest, src, (nb_elems) * sizeof(*dest)))
#define memdup(array, size) \
   memcpy(malloc(size), array, size)
#define hm_memdup(array, size) \
   (opt_cast(&*(array)) memcpy(malloc((size) * sizeof(*(array))), array, (size) * sizeof(*(array))))

#define NewPtr(type, var_name, ...) \
   type * var_name = cnew(type, __VA_ARGS__)
#define NewPtrDim(type, var_name, dim1, ...) \
   type (* var_name)__VA_ARGS__ = (type (* )__VA_ARGS__)malloc(sizeof(type dim1 __VA_ARGS__))
#define InitPtr(type, var_name, ...) \
   type * var_name = allocT(type, __VA_ARGS__)
#define InitPtrArray(type, var_name, ...) \
   type * var_name = alloc_arrayT(type, __VA_ARGS__)

#define ScopedVaList(prev_arg, ap_name, ...) \
   { \
      va_list ap_name; \
      va_start(ap_name, prev_arg); \
      __VA_ARGS__ \
      va_end(ap_name); \
   }


#ifndef __cplusplus
   FUNC inline void swap_blocks(void * val1, void * val2, size_t size)
   {
      byte tmp[size];
      memcpy(tmp, val1, size);
      memcpy(val1, val2, size);
      memcpy(val2, tmp, size);
   }
   #define swap(a, b) swap_blocks(a, b, sizeof(*(a)))
#else
   template <typename T>
   void swap(T * val1, T * val2)
   { std::swap(*val1, *val2); }
#endif
#define assign_if(var, cond, value) block({ if (*(var) cond) { *(var) = value; } })
#define assign_gt(var, value) block({ if (*(var) < (value)) { *(var) = value; } })
#define assign_lt(var, value) block({ if (*(var) > (value)) { *(var) = value; } })
#define order(a, b) block({ if (*(a) > *(b)) { swap(a, b); } })


#define case_block(val, ...) case val: { __VA_ARGS__ break; }

#define hm_switch(expr) \
   { \
      typedef typeof(expr) _type; \
      struct { \
         _type val, tmp; \
      } _data = { expr };
#define hm_switchT(type, expr) \
   { \
      typedef type _type; \
      struct { \
         _type val, tmp; \
      } _data = { expr };
#define str_switch(expr) \
   { \
      typedef char const * _type; \
      struct { \
         _type val, tmp; \
         bool val_len_initialized; \
         size_t val_len, tmp_len; \
      } _data = { expr };
#define case_start if (false)
#define case_default else

#define case_eq(expr) \
   else if (_data.val == (expr))
#define case_neq(op_n_expr) \
   else if (_data.val op_n_expr)
#define case_between(expr_n_op1, op_n_expr2) \
   else if (expr_n_op1 _data.val && _data.val op_n_expr2)
#define case_one_of(...) \
   else if (index_of_as_ptr(&_data.val, ARRAY(_type, { __VA_ARGS__ }), \
         sizeof(_data.val), lit_array_len(_type, { __VA_ARGS__ })) >= 0)
#define case_struct(...) \
   else if (memcmp(&_data.val, (_data.tmp = COMP_LIT(_type, { __VA_ARGS__ }), &_data.tmp), sizeof(_data.val)) == 0)
#define case_struct_eq(expr) \
   else if (memcmp(&_data.val, ARRAY(_type, { expr }), sizeof(_data.val)) == 0)

#define case_str(expr) \
   else if (strcmp(_data.val, expr) == 0)
#define case_str_not(expr) \
   else if (strcmp(_data.val, expr) != 0)
#define case_str_one_of(...) \
   else if (str_is_among(_data.val, { __VA_ARGS__ }))
#define case_str_none_of(...) \
   else if (!str_is_among(_data.val, { __VA_ARGS__ }))
#define case_str_contains(expr) \
   else if (strstr(_data.val, expr) != nullptr)
#define case_str_prefix(expr) \
   else if (has_prefix(_data.val, expr))
#define case_str_suffix(expr) \
   else if ((_data.val_len_initialized ? 0 : (_data.val_len = strlen(_data.val)), _data.val_len_initialized = true), \
         _data.tmp = expr, _data.tmp_len = strlen(_data.tmp), \
         _data.tmp_len <= _data.val_len && strcmp(_data.val + _data.val_len - _data.tmp_len, _data.tmp) == 0)


#define for_range(type, var_name, init, cond, update) \
   for (type var_name = (init); var_name cond; var_name update)

#define for_size(type, var_name, array, nb_elems) \
   for (struct { type const * ptr; type const * end_ptr; bool finished; } _data = { array, _data.ptr + (nb_elems) }; \
         !_data.finished; _data.finished = true) \
      for (type var_name; _data.ptr != _data.end_ptr && (var_name = *_data.ptr, true); ++_data.ptr)
#define for_array(type, var_name, array) \
   for_size (type, var_name, array, array_len(array))

#ifndef __cplusplus
   #define for_values(type, var_name, ...) \
      for_size (type, var_name, ARRAY(type, __VA_ARGS__), lit_array_len(type, __VA_ARGS__))
#else
   #define for_values(type, var_name, ...) \
      for (type var_name: __VA_ARGS__)
#endif

#define for_size_ref(type, var_name, array, nb_elems) \
   for (type * var_name = array; var_name != nullptr; var_name = nullptr) \
      for (type const * _end_ptr = var_name + (nb_elems); var_name != _end_ptr; ++var_name)
#define for_array_ref(type, var_name, array) \
   for_size_ref (type, var_name, array, array_len(array))

#define for_array_ref_until(type, var_name, array, terminator) \
   for (type * var_name = array; *var_name != (terminator); ++var_name)
#define for_array_until(type, var_name, array, terminator) \
   for (type const * _ptr = array; _ptr != nullptr; _ptr = nullptr) \
      for (type var_name; (var_name = *_ptr) != (terminator); ++_ptr)

#define for_in(type, var_name, generator, terminator) \
   for (type var_name; (var_name = (generator)) != (terminator);)

#define for_slice_ref(type, var_name, slice) \
   for_size_ref (type, var_name, (slice).data, (slice).len)
#define for_slice(type, var_name, slice) \
   for_size (type, var_name, (slice).data, (slice).len)

#define for_list_ref(type, var_name, list_ptr) \
   for (List(type) * list_holder(var_name) = list_ptr, _next = *list_holder(var_name); _next != nullptr; _next = nullptr) \
      for (type * var_name; _next != nullptr && (var_name = &_next->item, _next = _next->next, true); \
         *list_holder(var_name) != _next ? (void)(list_holder(var_name) = &(*list_holder(var_name))->next) : (void)0)
#define for_list(type, var_name, list) \
   for (ListConst(type) _item = (ListConst(type))(list); _item != nullptr; _item = nullptr) \
      for (type var_name; _item != nullptr && (var_name = _item->item, true); _item = _item->next)


FUNC inline long index_of_as_ptr(void const * value, void const * array, size_t value_size, size_t nb_elems)
{
   for (size_t value_num = 0; value_num < nb_elems; ++value_num) {
      if (memcmp(value, (byte const *)array + value_num * value_size, value_size) == 0) {
         return (long)value_num;
      }
   }
   return -1;
}
#define index_of(type, value, ...) \
   index_of_as_ptr(ARRAY(type, { value }), ARRAY(type, __VA_ARGS__), sizeof(type), lit_array_len(type, __VA_ARGS__))
#define index_in_array(type, value, array, nb_elems) \
   indef_of_as_ptr(ARRAY(type, { value }), array, sizeof(type), nb_elems)
#define index_in_slice(type, value, slice) \
   indef_of_as_ptr(ARRAY(type, { value }), (slice).data, sizeof(type), (slice).len)
   
#define is_among(type, value, ...) (index_of(type, value, __VA_ARGS__) >= 0)

FUNC inline long index_in_str_array(char const * str, char const * const array[], size_t nb_elems)
{
   for (size_t str_num = 0; str_num < nb_elems; ++str_num) {
      if (array[str_num] != NULL && strcmp(str, array[str_num]) == 0) {
         return (long)str_num;
      }
   }
   return -1;
}
#define index_of_str(str, ...) \
   index_in_str_array(str, ARRAY(char const *, __VA_ARGS__), lit_array_len(char const *, __VA_ARGS__))
#define index_in_str_slice(str, slice) index_in_str_array(str, (slice).data, (slice).len)

#define str_is_among(str, ...) (index_of_str(str, __VA_ARGS__) >= 0)


FUNC bool has_prefix(char const * str, char const * prefix);

FUNC inline bool has_suffix(char const * str, char const * suffix)
{
   size_t str_len = strlen(str), suffix_len = strlen(suffix);
   return str_len >= suffix_len && strcmp(str + str_len - suffix_len, suffix) == 0;
}

#if !(defined __STDC_VERSION__ && __STDC_VERSION__ >= 202311L)
   FUNC inline char * hm_strdup(char const * str)
   { return (char *)memdup(str, (strlen(str) + 1) * sizeof(*str)); }
   #define strdup hm_strdup
#endif


#ifndef __cplusplus
   #define DeclComparator(type, suffix) \
      FUNC inline int cmp_ ## suffix(void const * ptr1, void const * ptr2) \
      { \
         type val1 = *(type const *)ptr1; type val2 = *(type const *)ptr2; \
         return val1 < val2 ? -1 : val1 > val2 ? 1 : 0; \
      }
   #define DefComparator(type, suffix) \
      FUNC extern inline int cmp_ ## suffix(void const * ptr1, void const * ptr2);

   DeclComparator(int, i)
   DeclComparator(long, l)
   DeclComparator(float, ff)
   DeclComparator(double, f)
   DeclComparator(long double, fl)
   DeclComparator(size_t, z)

   #define HM_COMPARATORS \
      int: cmp_i, long: cmp_l, float: cmp_ff, double: cmp_f, long double: cmp_fl, size_t: cmp_z
   #define comparator_for(value) _Generic(value, HM_COMPARATORS)

#else
   template <typename T>
   int Comparator(T const * ptr1, T const * ptr2)
   { return *ptr1 < *ptr2 ? -1 : *ptr1 > *ptr2 ? 1 : 0; }

   #define comparator_for(value) ((int (* )(void const *, void const *))&Comparator<typeof(value)>)
#endif

#define DeclMinMax(type, prefix) \
   FUNC inline type prefix ## min(type val1, type val2) \
   { return val1 < val2 ? val1 : val2; } \
   FUNC inline type prefix ## max(type val1, type val2) \
   { return val1 > val2 ? val1 : val2; }
#define DefMinMax(type, prefix) \
   FUNC extern inline type prefix ## min(type val1, type val2); \
   FUNC extern inline type prefix ## max(type val1, type val2);

DeclMinMax(int, i)
DeclMinMax(long, l)
DeclMinMax(size_t, z)

#define hm_qsort(array) qsort(array, array_len(array), sizeof(*(array)), comparator_for(*(array)))
#define hm_bsearch(value, array) \
   (opt_cast(value) bsearch(value, array, array_len(array), sizeof(*(array)), comparator_for(*(array))))


#define all_bits_on(array) memset(array, 0xFF, sizeof(array))
#define all_bits_off(array) memset(array, 0, sizeof(array))
#define all_zero(array) all_bits_off(array)

#define BITS_PER_SLOT (sizeof(bits_t) * CHAR_BIT)
typedef uint_fast8_t bits_t;

#define bitmask(i) ((bits_t)1 << ((i) % BITS_PER_SLOT))
#define bitslot(i) ((i) / BITS_PER_SLOT)
#define bit_on(arr, i) ((void)((arr)[bitslot(i)] |= bitmask(i)))
#define bit_off(arr, i) ((void)((arr)[bitslot(i)] &= ~bitmask(i)))
#define bit_to(arr, i, val) (val ? bit_on(arr, i) : bit_off(arr, i))
#define bit(arr, i) (((arr)[bitslot(i)] & bitmask(i)) != 0)
#define bitnslots(nb) (((nb) + BITS_PER_SLOT - 1) / BITS_PER_SLOT)


/*--- Containers ---*/

#define DeclareSlice(type) \
   typedef struct Slice(type) { size_t len; type * data; } Slice(type); \
   typedef struct SliceConst(type) { size_t len; type const * data; } SliceConst(type);
#define Slice(type) Slice_ ## type
#define SliceConst(type) SliceConst_ ## type

DeclareSlice(void)

#define slice_new(type, nb_elems) (Slice(type)){ nb_elems, cnew(type, [nb_elems]) }
#define slice_build(type, ...) \
   (Slice(type)){ lit_array_len(type, { __VA_ARGS__ }), \
      hm_memdup(ARRAY(type, { __VA_ARGS__ }), lit_array_len(type, { __VA_ARGS__ })) }
#define slice_dup(slice) (typeof(slice)){ (slice).len, hm_memdup((slice).data, (slice).len) }
#define slice_dupT(type, slice) (Slice(type)){ (slice).len, hm_memdup((slice).data, (slice).len) }

#define slice_on_array(type, array) (Slice(type)){ array_len(array), array }
#define slice_on_values(type, ...) (Slice(type)){ lit_array_len(type, { __VA_ARGS__ }), ARRAY(type, { __VA_ARGS__ }) }
#define slice_on(type, slice) (Slice(type)){ (slice).len, (slice).data }

FUNC void * _array_of_list(void const * list, size_t item_size, size_t offset);
#define slice_of_list(type, list) (Slice(type)){ list_len(list), _array_of_list(list, sizeof(type), list_item_offset(type)) }

FUNC inline void _slice_realloc(Slice(void) * slice_addr, size_t nb_elems, size_t item_size)
{
   slice_addr->data = realloc(slice_addr->data, sizeof_dims(byte, [nb_elems][item_size]));
   slice_addr->len = nb_elems;
}
#define slice_realloc(slice_addr, nb_elems) _slice_realloc((Slice(void) *)(slice_addr), nb_elems, sizeof((slice_addr)->data))
FUNC inline void _slice_assign(Slice(void) * slice_addr, SliceConst(void) * other_slice, size_t item_size)
{
   free(slice_addr->data);
   slice_addr->len = other_slice->len;
   slice_addr->data = memdup(other_slice->data, sizeof_dims(byte, [other_slice->len][item_size]));
}
#define slice_assign(slice_addr, other_slice) \
   _slice_assign((Slice(void) *)(slice_addr), (SliceConst(void) *)&other_slice, sizeof((slice_addr)->data))

FUNC inline void _slice_memset(Slice(void) * slice, int val, size_t item_size)
{ memset(slice->data, val, slice->len * item_size); }
#define slice_all_bits_on(slice) _slice_memset((Slice(void) *)&slice, 0xFF, sizeof(*(slice).data))
#define slice_all_bits_off(slice) _slice_memset((Slice(void) *)&slice, 0, sizeof(*(slice).data))
#define slice_all_zero(slice) slice_all_bits_off(slice)

FUNC inline void _slice_qsort(Slice(void) * slice, size_t item_size, int (* comparator)(void const *, void const *))
{ qsort(slice->data, slice->len, item_size, comparator); }
#define slice_qsort(slice) _slice_qsort((Slice(void) *)&slice, sizeof(*(slice).data), comparator_for(*(slice).data))
FUNC inline void * _slice_bsearch(void const * value, SliceConst(void) * slice, size_t item_size,
      int (* comparator)(void const *, void const *))
{ return bsearch(value, slice->data, slice->len, item_size, comparator); }
#define slice_bsearch(value, slice) \
   (opt_cast(value) _slice_bsearch(value, (SliceConst(void) *)&slice, sizeof(*(slice).data), comparator_for(*(slice).data)))


#define DeclareDynArr(type) \
   typedef struct DynArr(type) { size_t len; type * data; size_t capac; } DynArr(type); \
   typedef struct DynArrConst(type) { size_t len; type const * data; size_t capac; } DynArrConst(type);
#define DynArr(type) DynArr_ ## type
#define DynArrConst(type) DynArrConst_ ## type

DeclareDynArr(void)

#define dynarr_new(type, nb_elems) (DynArr(type)){ nb_elems, cnew(type, [nb_elems]), nb_elems }
#define dynarr_dup(dynarr) (typeof(dynarr)){ (dynarr).len, hm_memdup((dynarr).data, (dynarr).capac), (dynarr).capac }
#define dynarr_dupT(type, dynarr) (DynArr(type)){ (dynarr).len, hm_memdup((dynarr).data, (dynarr).capac), (dynarr).capac }

FUNC void _dynarr_grow(DynArr(void) * dynarr_addr, size_t item_size);
#define dynarr_grow(dynarr_addr) _dynarr_grow((DynArr(void) *)(dynarr_addr), sizeof(*(dynarr_addr)->data))

FUNC inline void _dynarr_shrink(DynArr(void) * dynarr_addr, size_t item_size)
{
   dynarr_addr->capac = dynarr_addr->len;
   dynarr_addr->data = realloc(dynarr_addr->data, sizeof_dims(byte, [dynarr_addr->capac][item_size]));
}
#define dynarr_shrink(dynarr_addr) _dynarr_shrink((DynArr(void) *)(dynarr_addr), sizeof(*(dynarr_addr)->data))

FUNC inline void _dynarr_push(DynArr(void) * dynarr_addr, void const * values, size_t nb_vals, size_t item_size)
{
   dynarr_addr->len += nb_vals;
   _dynarr_grow(dynarr_addr, item_size);
   memcpy((byte *)dynarr_addr->data + (dynarr_addr->len - nb_vals) * item_size, values, nb_vals * item_size);
}
#define dynarr_push(dynarr_addr, ...) \
   _dynarr_push((DynArr(void) *)(dynarr_addr), ARRAY(typeof(*(dynarr_addr)->data), { __VA_ARGS__ }), \
      lit_array_len(typeof(*(dynarr_addr)->data), { __VA_ARGS__ }), sizeof(*(dynarr_addr)->data))
#define dynarr_pushT(type, dynarr_addr, ...) \
   _dynarr_push((DynArr(void) *)(dynarr_addr), ARRAY(type, { __VA_ARGS__ }), lit_array_len(type, { __VA_ARGS__ }), sizeof(type))
#define dynarr_cat(dynarr_addr, slice) \
   _dynarr_push((DynArr(void) *)(dynarr_addr), (slice).data, (slice).len, sizeof(*(dynarr_addr)->data))

FUNC void _dynarr_pop(DynArr(void) * dynarr_addr, size_t item_size);
#define dynarr_pop(dynarr_addr) _dynarr_pop((DynArr(void) *)(dynarr_addr), sizeof(*(dynarr_addr)->data))



#define DeclareDeque(type) \
   typedef struct Deque(type) { size_t len; type * data, * buf; size_t capac; } Deque(type); \
   typedef struct DequeConst(type) { size_t len; type const * data, * buf; size_t capac; } DequeConst(type);
#define Deque(type) Deque_ ## type
#define DequeConst(type) DequeConst_ ## type

DeclareDeque(void)

FUNC void _deque_grow_after(Deque(void) * deque_addr, size_t item_size);
#define deque_grow_after(deque_addr) _deque_grow_after((Deque(void) *)(deque_addr), sizeof(*(deque_addr)->data))
FUNC void _deque_grow_before(Deque(void) * deque_addr, size_t item_size);
#define deque_grow_before(deque_addr) _deque_grow_before((Deque(void) *)(deque_addr), sizeof(*(deque_addr)->data))

FUNC void _deque_shrink(Deque(void) * deque_addr, size_t item_size);
#define deque_shrink(deque_addr) _deque_shrink((Deque(void) *)(deque_addr), sizeof(*(deque_addr)->data))

FUNC inline void _deque_push(Deque(void) * deque_addr, void const * values, size_t nb_vals, size_t item_size)
{
   deque_addr->len += nb_vals;
   _deque_grow_after(deque_addr, item_size);
   memcpy((byte *)deque_addr->data + (deque_addr->len - nb_vals) * item_size, values, nb_vals * item_size);
}
#define deque_push(deque_addr, ...) \
   _deque_push((Deque(void) *)(deque_addr), ARRAY(typeof(*(deque_addr)->data), { __VA_ARGS__ }), \
      lit_array_len(typeof(*(deque_addr)->data), { __VA_ARGS__ }), sizeof(*(deque_addr)->data))
#define deque_pushT(type, deque_addr, ...) \
   _deque_push((Deque(void) *)(deque_addr), ARRAY(type, { __VA_ARGS__ }), \
      lit_array_len(type, { __VA_ARGS__ }), sizeof(*(deque_addr)->data))
#define deque_cat(deque_addr, slice) \
   _deque_push((Deque(void) *)(deque_addr), (slice).data, (slice).len, sizeof(*(deque_addr)->data))

FUNC void _deque_pop(Deque(void) * deque_addr, size_t item_size);
#define deque_pop(deque_addr) _deque_pop((Deque(void) *)(deque_addr), sizeof(*(deque_addr)->data))

FUNC void _deque_shift(Deque(void) * deque_addr, size_t item_size);
#define deque_shift(deque_addr) _deque_shift((Deque(void) *)(deque_addr), sizeof(*(deque_addr)->data))

FUNC inline void _deque_unshift(Deque(void) * deque_addr, void const * value, size_t item_size)
{
   ++deque_addr->len;
   _deque_grow_before(deque_addr, item_size);
   deque_addr->data = (byte *)deque_addr->data - item_size;
   memcpy((byte *)deque_addr->data, value, item_size);
}
#define deque_unshift(deque_addr, value) \
   _deque_unshift((Deque(void) *)(deque_addr), ARRAY(typeof(*(deque_addr)->data), { value }), sizeof(*(deque_addr)->data))
#define deque_unshiftT(type, deque_addr, value) \
   _deque_unshift((Deque(void) *)(deque_addr), ARRAY(type, { value }), sizeof(*(deque_addr)->data))


#define DeclareList(type) \
   typedef struct List(type ## _) * List(type); \
   struct List(type ## _) { List(type) next; type item; }; \
   typedef struct ListConst(type ## _) const * ListConst(type); \
   struct ListConst(type ## _) { ListConst(type) next; type const item; };
#define List(type) List_ ## type
#define ListConst(type) ListConst_ ## type

#define list_item_offset(type) offsetof(struct List(type ## _), item)
#define list_holder(var_name) _ ## var_name ## _holder

DeclareList(byte)

FUNC void * _list_of_array(void const * array, size_t nb_elems, size_t item_size, size_t offset, size_t full_size);
#define list_of_values(type, ...) \
   ((List(type))_list_of_array(ARRAY(type, { __VA_ARGS__ }), lit_array_len(type, { __VA_ARGS__ }), sizeof(type), \
      list_item_offset(type), sizeof(struct List(type ## _))))
#define list_of_array(type, array) \
   ((List(type))_list_of_array(array, array_len(array), sizeof(type), list_item_offset(type), sizeof(struct List(type ## _))))
FUNC inline void * _list_of_slice(SliceConst(void) slice, size_t item_size, size_t offset, size_t full_size)
{ return _list_of_array(slice.data, slice.len, item_size, offset, full_size); }
#define list_of_slice(type, slice) \
   ((List(type))_list_of_array(*(SliceConst(void))&slice, sizeof(type), list_item_offset(type), sizeof(struct List(type ## _))))

FUNC inline void * * _list_cons(void const * item, size_t item_size, void * list_addr, size_t offset, size_t full_size)
{
   List(byte) prev = (List(byte))malloc(full_size);
   memcpy(&prev->next, list_addr, sizeof(prev->next));
   *(void * *)list_addr = prev;
   Cast(List(byte), list, *(void * *)list_addr);
   memcpy((byte *)list + offset, item, item_size);
   return (void * *)list_addr;
}
#define list_cons(value, list_addr) \
   ((typeof(list_addr))_list_cons(ARRAY(typeof((*(list_addr))->item), { value }), sizeof((*(list_addr))->item), list_addr, \
      offsetof(typeof(**(list_addr)), item), sizeof(**(list_addr))))
#define list_consT(type, value, list_addr) \
   ((List(type) *)_list_cons(ARRAY(type, { value }), sizeof(type), list_addr, list_item_offset(type), sizeof(**(list_addr))))

FUNC inline void * * _list_decons(void * list_addr)
{
   void * prev = *(void * *)list_addr;
   *(void * *)list_addr = ((List(byte))prev)->next;
   free(prev);
   return (void * *)list_addr;
}
#define list_decons(list_addr) (opt_cast(list_addr) _list_decons(list_addr))
#define list_deconsT(type, list_addr) ((List(type) *)_list_decons(list_addr))

FUNC void * * _list_cat(void * list1_addr, void * list2);
#define list_cat(list1_addr, list2) (opt_cast(list1_addr) _list_cat(list1_addr, list2))
#define list_catT(type, list1_addr, list2) ((List(type) *)_list_cat(list1_addr, list2))
#define list_prepend(values, list_addr) \
   ((void)(*(list_addr) = *(typeof(list_addr))_list_cat( \
      LVAL_ARRAY(void *, { _list_of_array( \
         ARRAY(typeof((*(list_addr))->item), { va_args(values) }), \
         sizeof((*(list_addr))->item), lit_array_len(typeof((*(list_addr))->item), { va_args(values) }), \
         offsetof(typeof(**(list_addr)), item), sizeof(**(list_addr))) }), \
      *(list_addr))))
#define list_prependT(type, values, list_addr) \
   ((void)(*(list_addr) = *(List(type) *)_list_cat(LVAL_ARRAY(void *, { list_of_values(type, va_args(values)) }), *(list_addr))))

FUNC void list_free(void * list);

FUNC size_t list_len(void const * list);
FUNC size_t list_len_bounded(void const * list, size_t limit);


#define DeclareDictEntry(type) \
   typedef struct DictEntry(type) { char const * key; type val; } DictEntry(type);
#define DictEntry(type) DictEntry_ ## type
#define Dict(type) Dict_ ## type

#define dict_new(type, nb_elems) slice_new(DictEntry(type), nb_elems)
#define dict_build(type, ...) slice_build(DictEntry(type), __VA_ARGS__)

FUNC void * _dict_get(void * data, size_t nb_elems, char const * key, size_t item_size, size_t offset);
#define dict_get(dict, key) \
   (opt_cast(&(dict).data->val) _dict_get((dict).data, (dict).len, key, sizeof(*(dict).data), offsetof_inobj(*(dict).data, val)))

FUNC void _dict_set(DynArr(void) * dict, char const * key, void * value, size_t item_size, size_t offset, size_t value_size);
#define dict_set(dict, key, value) \
   _dict_set((DynArr(void) *)dict, key, LVAL_ARRAY(typeof((dict)->data->val), { value }), \
      sizeof(*(dict)->data), offsetof_inobj(*(dict)->data, val), sizeof((dict)->data->val))
#define dict_setT(type, dict, key, value) \
   _dict_set((DynArr(void) *)dict, key, LVAL_ARRAY(type, { value }), \
      sizeof(*(dict)->data), offsetof_inobj(*(dict)->data, val), sizeof(type))


#endif
