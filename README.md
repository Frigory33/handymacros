# HandyMacros

## Install

```
make
sudo make install
```

To uninstall:
```
sudo make uninstall
```

To install/uninstall elsewhere, use `prefix=`:
```
make install prefix=~/.local
```

You can also simply include the contents of the `src` folder in your project.

## Test

```
make run-test
```

## Features

This small library provides macros to get some high-level features in the C language. This is hard-to-read code to make your
projects’ code more beautiful, easier to read and following some high-level standards. It spares including some boilerplate code
in all of your projects, still enjoying the speed of C.

- Shortcuts for types: array size, casts
- Macros to make functions with optional arguments
- Shortcuts for allocation
- More high-level `switch`
- For-each loops
- Comparators, min/max for scalar types
- Manipulation of arrays of bits
- Containers:
  - Slice
  - Dynamic array
  - Deque
  - Singly linked list

Most features are C99-compatible, some require C11 or C23.

## Documentation

Look at the tests, and directly inside `handymacros.h`.

Compile with `-lhandymacros`.
