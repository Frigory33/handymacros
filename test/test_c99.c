#include "test.h"


TestData test_data;


void array_testing(void)
{
   section_header("Array testing");

   char const * me = "me";
   for_values (char const *, word, { "Hello", "world", "this", "is", me }) {
      printf("%s:%zu:", word, strlen(word));
      for_array_until (char, crt, word, '\0') {
         printf("%c", toupper(crt));
      }
      printf(" ");
   }
   printf("\n\n");

   int my_ints[16];
   for_range (size_t, i, 0, < array_len(my_ints), ++) {
      my_ints[i] = i;
   }
   for_array (int, my_int, my_ints) {
      printf("%2d ", my_int);
   }
   for_array_ref (int, my_int_ptr, my_ints) {
      ++*my_int_ptr;
   }
   printf("\n\n");
   PtrDim2(int, my_ints_table, [4], [4], my_ints);
   for_range (int, i1, 0, < 4, ++) {
      for_range (int, i2, 0, < 4, ++) {
         printf("%2d ", my_ints_table[i1][i2]);
      }
      printf("\n");
   }
   printf("\n");
   PtrDim2(int, my_ints_4d, [2], [2][2][2], my_ints);
   for_range (int, i1, 0, < 2, ++) {
      for_range (int, i2, 0, < 2, ++) {
         printf("%4c", ' ');
         for_range (int, i3, 0, < 2, ++) {
            for_range (int, i4, 0, < 2, ++) {
               printf("%2d ", my_ints_4d[i1][i2][i3][i4]);
            }
            printf("%4c", ' ');
         }
         printf("\n");
      }
      printf("\n");
   }

   short my_shorts_4d[2][2][2][2];
   printf("4D-array of 2×2×2×2 elements contains %zu elements in total\n\n", md_array_len(my_shorts_4d, ****));

   float my_floats[24];
   for_range (size_t, i, 0, < array_len(my_floats), ++) {
      my_floats[i] = i * .17f;
   }
   for_array (float, my_float, my_floats) {
      printf("%4g ", my_float);
   }
   printf("\n\n");
   PtrDim2(float, my_floats_3d, [2], [3][4], my_floats);
   for_range (int, i1, 0, < 2, ++) {
      for_range (int, i2, 0, < 3, ++) {
         for_range (int, i3, 0, < 4, ++) {
            printf("%4g ", my_floats_3d[i1][i2][i3]);
         }
         printf("%4c", ' ');
      }
      printf("\n");
   }
   printf("\n");

   for_values (char const *, word, { "This", "is", "not", "fin...", "ished" }) {
      printf("%s ", word);
      if (has_suffix(word, "...")) {
         break;
      }
   }
   printf("\n");
}


void fancy_switch(void)
{
   section_header("Fancy switch");

   init_input("0 1 2 6 7 32 42 50 56 -1 2048 -2048 101");
   while (!input_end()) {
      printf("Type an integer number between 0 and 100: ");
      int nb;
      scanf("%d", &nb);
      hm_switchT (int, nb) {
         case_start;
         case_eq (0) {
            printf("   This is null\n");
         } case_eq (1) {
            printf("   You typed one\n");
         } case_eq (2) {
            printf("   You typed two\n");
         } case_between (3 <=, <= 6) {
            printf("   Common dies have this figure\n");
         } case_eq (7) {
            printf("   Some say it’s a lucky number\n");
         } case_one_of (8, 16, 32, 64) {
            printf("   A power of 2, thank you, I love it\n");
         } case_between (8 <, <= 100) {
            printf("   %d + %d = 100\n", nb, 100 - nb);
            case_start;
            case_eq (42) {
               printf("   That is the question… uh… the answer\n");
            }
         } case_default {
            printf("   Hey, it’s not in the range!!\n");
         }
         case_start;
         case_neq (< 0) {
            printf("   It’s negative\n");
         } case_neq (< 50) {
            printf("   It’s below 50\n");
         } case_eq (50) {
            printf("   It’s just 50\n");
         } case_neq (<= 100) {
            printf("   It’s above 50\n");
         } case_default {
            printf("   It’s just too much\n");
         }
      } hm_end
   }
   printf("\n");

   init_input("handy macros mmmmh");
   while (!input_end()) {
      printf("Let’s try with a string: ");
      char str[32];
      scanf("%31s", str);
      str_switch (str) {
         case_start;
         case_str ("handy") {
            printf("   This is an adjective\n");
         } case_str ("macros") {
            printf("   This is a noun in plural form\n");
         } case_default {
            printf("   What?????\n");
         }
         case_start;
         case_str_prefix ("hand") {
            printf("   This starts the name of the library\n");
         } case_str_prefix ("m") {
            printf("   It starts with the letter M which usually has a soft sound\n");
         }
         case_start; case_str_suffix ("os") {
            printf("   It finishes like a Spanish word in masculine plural\n");
         }
         case_start; case_str_contains ("a") {
            printf("   It contains the first vowel of the latin alphabet\n");
         }
         case_start; case_str_one_of ("handy", "macros") {
            printf("   This word is in the name of the library\n");
         }
      } hm_end
   }
   printf("\n");

#ifndef __cplusplus
   #define case_struct_2(first, ...) case_struct(__VA_ARGS__, first)
#else
   #define case_struct_2 case_struct
#endif
   Struct(ForTesting, {
      char c[3];
      byte padding;
      int32_t i;
   })
   init_input("ab 1 ef 2 gh 3");
   while (!input_end()) {
      printf("2 chars and an int, please: ");
      ForTesting for_test = { .padding = 0 };
      static ForTesting const some_val = { .c = "ef", .i = 2 };
      scanf("%2c %d", for_test.c, &for_test.i);
      hm_switchT (ForTesting, for_test) {
         case_start;
         case_struct_2 (.c = "ab", .i = 1) {
            printf("You found the first case\n");
         } case_struct_eq (some_val) {
            printf("This is the second case\n");
         } case_default {
            printf("This is nothing\n");
         }
      } hm_end
   }
}


void _show_all_strings(char const * first_str, ...)
{
   section_header("Show all strings (va_list)");
   printf("%s\n", first_str);
   ScopedVaList(first_str, arg_ptr, {
      for_in (char const *, str, va_arg(arg_ptr, char const *), nullptr) {
         printf("%s\n", str);
      }
   })
}
#define show_all_strings(...) _show_all_strings(__VA_ARGS__, nullptr)


void sieve_of_eratosthenes(void)
{
   section_header("Sieve of Eratosthenes");
   int const lastNb = 10000;
   bits_t hasDivisor[bitnslots(lastNb + 1)];
   all_bits_off(hasDivisor);
   for (int nb = 2; nb <= lastNb; ++nb) {
      if (!bit(hasDivisor, nb)) {
         printf("%5d", nb);
         for (int mult = nb * 2; mult <= lastNb; mult += nb) {
            bit_on(hasDivisor, mult);
         }
      }
   }
   printf("\n");
}


DeclareDeque(int)

static void print_int_deque_stats(Deque(int) int_deque)
{
   size_t neg_excess = int_deque.data - int_deque.buf, pos_excess = int_deque.capac - neg_excess - int_deque.len;
   printf("(%3zu|%3zu|%3zu|%3zu)   ", int_deque.len, int_deque.capac, neg_excess, pos_excess);
}

void lets_deque(void)
{
   section_header("Let’s deque");
   Deque(int) int_deque = { 0 };
   printf("Length|Capacity|Neg excess|Pos excess\n");
   print_int_deque_stats(int_deque);
   printf("\n");
   for_range (int, nb, 0, < 20, ++) {
      deque_pushT(int, &int_deque, nb);
      print_int_deque_stats(int_deque);
   }
   printf("\n");
   for_range (int, nb, 0, < 20, ++) {
      deque_unshiftT(int, &int_deque, nb);
      print_int_deque_stats(int_deque);
   }
   printf("\n");
   for_range (int, nb, 0, < 20, ++) {
      deque_pop(&int_deque);
      print_int_deque_stats(int_deque);
   }
   printf("\n");
   for_range (int, nb, 0, < 15, ++) {
      deque_shift(&int_deque);
      print_int_deque_stats(int_deque);
   }
   printf("\nFinal contents:");
   for_slice (int, nb, int_deque) {
      printf(" %d", nb);
   }
   printf("\n");
   free(int_deque.buf);
}


DeclareList(int)
DeclareList(long_double)

static void print_int_list(ListConst(int) list)
{
   for_list (int, item, list) {
      printf("%d ", item);
   }
   if (list == nullptr) {
      printf("(nothing)");
   }
   printf("\n");
}

void holy_lists(void)
{
   section_header("Holy lists");
   List(int) int_list = nullptr;
   list_consT(int, 99, list_consT(int, 27, &int_list));
   list_prependT(int, (13, 666, 8000), &int_list);
   list_consT(int, 42, &int_list->next);
   print_int_list((ListConst(int))int_list);
   list_decons(&int_list->next);
   print_int_list((ListConst(int))int_list);
   list_decons(list_deconsT(int, &int_list));
   print_int_list((ListConst(int))int_list);
   list_free(int_list);
   List(long_double) dbl_list = list_of_values(long_double, 1.23l, 4.56l, 7.89l);
   list_consT(long_double, -1.23l, &dbl_list);
   Cast(ListConst(long_double), dbl_list_const, dbl_list);
   for_list (long_double, item, dbl_list_const) {
      printf("%Lg ", item);
   }
   printf("\n");
   for_list_ref (long_double, item, &dbl_list) {
      --*item;
   }
   for_list (long_double, item, dbl_list_const) {
      printf("%Lg ", item);
   }
   printf("\n");
   list_free(dbl_list);
   Struct(ForTesting, {
      char const * str;
      bool b;
   })
   DeclareList(ForTesting)
   List(ForTesting) test_list = list_of_values(ForTesting, { "ab", true }, { "cd" }, { "ef" });
   for_list (ForTesting, item, test_list) {
      printf("%s|%c  ", item.str, item.b ? 'T' : 'F');
   }
   printf("\n");
   list_cat(&test_list, list_of_values(ForTesting, { "gh", true }, { "ij" }));
   for_list (ForTesting, item, test_list) {
      printf("%s|%c  ", item.str, item.b ? 'T' : 'F');
   }
   printf("\n");
   for_list_ref (ForTesting, item, &test_list) {
      if (!item->b) {
         list_decons(list_holder(item));
      }
   }
   for_list (ForTesting, item, test_list) {
      printf("%s|%c  ", item.str, item.b ? 'T' : 'F');
   }
   printf("\n");
   Struct(Vector2i, {
      int x, y;
   })
   DeclareList(Vector2i)
   List(Vector2i) pos_list = nullptr;
   list_cat(list_catT(Vector2i, list_catT(Vector2i, &pos_list,
      list_of_values(Vector2i, { 0, 0 }, { 1, -1 }, { 8, 3 })),
      list_of_values(Vector2i, { -10, -5 }, { 4, -9 })), list_of_values(Vector2i, { -6, 40 }));
   for_list (Vector2i, pos, pos_list) {
      printf("%d,%d  ", pos.x, pos.y);
   }
   printf("\n");
   Struct(WeirdIterator, {
      List(ForTesting) test;
      List(Vector2i) pos;
   })
   printf("Trying to parse both at once:  ");
   for (WeirdIterator mess = { test_list, pos_list }; mess.test != nullptr && mess.pos != nullptr;
         mess = COMP_LIT(WeirdIterator, { mess.test->next, mess.pos->next })) {
      printf("%s|%c %d,%d  ", mess.test->item.str, mess.test->item.b ? 'T' : 'F', mess.pos->item.x, mess.pos->item.y);
   }
   printf("(at least one of the lists has nothing else)\n");
   printf("Lengths: %zu %zu; cut at 4: %zu %zu\n", list_len(test_list), list_len(pos_list),
      list_len_bounded(test_list, 4), list_len_bounded(pos_list, 4));
   list_free(test_list);
   list_free(pos_list);
}


DeclareDictEntry(int)
DeclareDynArr(DictEntry_int)

void oh_my_dict(void)
{
   section_header("Oh my dict");
   DynArr(DictEntry_int) dict = { 0 };
   dict_setT(int, &dict, "One", 1);
   dict_setT(int, &dict, "Two", 2);
   printf("Contents of dict:");
   for_slice (DictEntry_int, ent, dict) {
      printf(" %s:%d", ent.key, ent.val);
   }
   printf("\n");
   printf("With “One”: %d\nWith “Two”: %d\n", *(int *)dict_get(dict, "One"), *(int *)dict_get(dict, "Two"));
   free(dict.data);
}


int main(void)
{
   main_header("C99-compatible tests");
   array_testing();
   fancy_switch();
   show_all_strings("Lorem ipsum", "Dolor sit amet", "What else?");
   sieve_of_eratosthenes();
   lets_deque();
   holy_lists();
   oh_my_dict();
   printf("\n");
}
