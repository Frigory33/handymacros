#include "test.h"


TestData test_data;


Function(void, show_three_values, (int a, b; double c))
{
   static int count = 0;
   ++count;
   printf("%3d) I got: %d, %d, %g\n", count, arg.a, arg.b, arg.c);
}
#define show_three_values(...) call_func(show_three_values, (0), (__VA_ARGS__))
#define show_three_values_2(a, ...) call_func(show_three_values, (.b = 2, 3.14), ((a), __VA_ARGS__))
#define show_three_values_3(a, b, ...) call_func(show_three_values, (.c = 1.61), ((a), (b), __VA_ARGS__))

void default_args(void)
{
   section_header("Default args");
   show_three_values();
   show_three_values_2(100);
   show_three_values_3(33, 42);
   show_three_values(8, .c = 7.13);
   show_three_values_2(50, 51);
   show_three_values_3(52, 53, 54.6);
   show_three_values(70, 71, 72.3);
   show_three_values_2(11, .c = 9.99);
   show_three_values_3(78, 74, .c = 75.6);
   show_three_values(.c = 101., .b = 102, .a = 103);
   show_three_values_2(104, .a = 105);
   show_three_values_3(666, 107, .c = 108.9, .a = 106);
}


int main(void)
{
   main_header("C23-compatible tests");
   default_args();
   printf("\n");
}
