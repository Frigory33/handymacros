#ifndef HM_TEST_H
#define HM_TEST_H


#include "handymacros.h"

#include <stdio.h>
#include <ctype.h>


#ifndef __cplusplus
   #define main_header(text) printf("\n##### %s (C : %ld) #####\n", text, __STDC_VERSION__)
#else
   #define main_header(text) printf("\n##### %s (C++ : %ld) #####\n", text, __cplusplus)
#endif

#define section_header(text) printf("\n***** %s *****\n", text)

#define init_input(input) \
   ((void)(test_data.input_string = input))
#define scanf(format, ...) \
   block({ \
      sscanf(test_data.input_string, format "%n", __VA_ARGS__, &test_data.input_read_len), \
      printf("%.*s\n", test_data.input_read_len, test_data.input_string), \
      test_data.input_string += test_data.input_read_len, \
      sscanf(test_data.input_string, " %n", &test_data.input_read_len), \
      test_data.input_string += test_data.input_read_len; \
   })
#define input_end() (*test_data.input_string == '\0')


Struct(TestData, {
   char const * input_string;
   int input_read_len;
})

extern TestData test_data;


#endif
