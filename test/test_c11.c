#include "test.h"


TestData test_data;


void sorting(void)
{
   section_header("Sorting");
   int values[10];
   printf("Unsorted array:");
   for_array_ref (int, val, values) {
      *val = rand() * 100. / (RAND_MAX + 1.);
      printf(" %d", *val);
   }
   int some_val = values[0];
   hm_qsort(values);
   printf("\nSorted array:");
   for_array (int, val, values) {
      printf(" %d", val);
   }
   printf("\nPosition of former first value (%d): %zu\n", some_val, (int *)hm_bsearch(&some_val, values) - values);
}


int main(void)
{
   main_header("C11-compatible tests");
   sorting();
   printf("\n");
}
